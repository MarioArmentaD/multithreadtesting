/**
 * 
 */
package main;

/**
 * @author Mario
 *
 */
public interface IMessagePrinter {

	public void printMessage(String messageToPrint);
	
}
