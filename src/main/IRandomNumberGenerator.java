/**
 * 
 */
package main;

/**
 * @author Mario
 *
 */
public interface IRandomNumberGenerator {
	
	public double generateNumber();

}
