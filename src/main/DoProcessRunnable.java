/**
 * 
 */
package main;

/**
 * @author Mario
 *
 */
public class DoProcessRunnable implements Runnable {

	private IRandomNumberGenerator randomNumberGenerator;
	private IMessagePrinter messagePrinter;
	private int numberOfExecution;
	
	public DoProcessRunnable(int numberOfExecution) {
		this.randomNumberGenerator = new RandomNumberGeneratorMath(); 
		this.messagePrinter = new MessagePrinterConsole();
		this.numberOfExecution = numberOfExecution;
	}
	
	@Override
	public void run() {		
		messagePrinter.printMessage(String.format("This is the [%d] iteration, Random number: ", numberOfExecution) + Double.toString(randomNumberGenerator.generateNumber()));
	}
	
	
}
