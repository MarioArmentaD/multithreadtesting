/**
 * 
 */
package main;

import java.time.Duration;
import java.time.LocalDateTime;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

/**
 * @author Mario
 *
 */
public class Main {

	/**
	 * 
	 */
	public Main() {
		// TODO Auto-generated constructor stub
	}

	/**
	 * @param args
	 */
	public static void main(String[] args) {

		int numberOfIterations = 100;
		LocalDateTime initialDateTimeSingle = LocalDateTime.now();
		for (int i = 0; i < numberOfIterations; i++) {
			new DoProcessRunnable(i+1).run();
		}
		LocalDateTime finalDatetimeSingle = LocalDateTime.now();

		ExecutorService executorService = Executors.newFixedThreadPool(2);
		LocalDateTime initialDateTime = LocalDateTime.now();
		for (int i = 0; i < numberOfIterations; i++) {
			executorService.execute(new DoProcessRunnable(i + 1));
		}
		executorService.shutdown();
		try {
			if (executorService.awaitTermination(2000, TimeUnit.SECONDS)) {
				LocalDateTime finalDatetime = LocalDateTime.now();
				System.out.println("Total time in milis (Multi Thread): " + Duration.between(initialDateTime, finalDatetime).toMillis());
				System.out.println("Total time in milis (Single Thread): " + Duration.between(initialDateTimeSingle, finalDatetimeSingle).toMillis());
			} else {
				System.out.println("Timeout, the executor took longer than expected");
			}
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		

	}

}
